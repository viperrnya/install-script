# INSTALL SCRIPTS

---

## About

---

I made these install scripts so I can get my Arch based system up and running in seconds rather than installing all packages manually every time.
- See my [dotfiles](https://gitlab.com/viperrnya/dotfiles) 

## Usage

---

**DISCLAIMER**
- This script is going to overwrite your dotfiles with mine!
- Make sure you don't have a directory named "dotfiles" in your home folder.

1. Install [git](https://github.com/git/git) if you don't already have it.
2. Clone the repo and `cd` into it using `git clone https://gitlab.com/viperrnya/install-script && cd install-script` in your terminal
3. After that type either `./install.sh` or `./install-full.sh` depending whether you want to install bare essentials from my config (useful for spinning up a VM) or my config plus programs I use on a daily basis (steam, spotify, discord, etc.)

## Notes

---

This script assumes you have already done the basic [arch install](https://wiki.archlinux.org/title/installation_guide) and you are logged into the tty.

## TODO

---
- [x] Add pipewire to install-full script
- [ ] Update install-full script once I further organize my dotfiles to include missing programs
- [ ] Implement mouse tweaks (fix scrolling, disable acceleration and enable side buttons)