#!/usr/bin/env bash
#------------------------------------------------------------------------------                                                                                                                                                     )  
#                                  )     (  (                         )  
#   (      (        (           ( /(   ) )\ )\         (  (        ( /(  
#  ))\`  ) )\  (    )\  (    (  )\()| /(((_|(_)  (   ( )( )\ `  )  )\()) 
# /((_)(/(((_) )\  ((_) )\ ) )\(_))/)(_))_  _    )\  )(()((_)/(/( (_))/  
#(_))((_)_\(_)((_)  (_)_(_/(((_) |_((_)_| || |  ((_)((_|(_|_|(_)_\| |_   
#/ -_) '_ \) / _|   | | ' \)|_-<  _/ _` | || |  (_-< _| '_| | '_ \)  _|  
#\___| .__/|_\__|   |_|_||_|/__/\__\__,_|_||_|  /__|__|_| |_| .__/ \__|  
#    |_|                                                    |_|                 
#------------------------------------------------------------------------------

echo
echo "INSTALLING PACMAN PACKAGES"
echo

PKGS=(

    # TERMINAL UTILITIES --------------------------------------------------

    'qtile'                  # Tiling window manager
    'htop'                   # Process viewer
    'neofetch'               # Shows system info in the terminal
    'neovim'                 # Terminal text editor
    'alacritty'              # Terminal emulator
    'starship'               # Prompt
    'ranger'                 # Terminal file manager

    # GENERAL UTILITIES ---------------------------------------------------

    'pcmanfm'                # Filesystem browser
    'nitrogen'               # Wallpaper changer
    'picom'                  # Compositor
    'pavucontrol'            # Audio control panel

    # DEVELOPMENT ---------------------------------------------------------

    'python'                 # Scripting language
    'python-pip'             # Python package manager

    # FONTS ---------------------------------------------------------------

    'ttf-ubuntu-font-family' # Used by qtile panel          
    'ttf-cascadia-code-nerd' # Used by alacritty and Starship prompt            
    'ttf-font-awesome'       # For compatibility
)

for PKG in "${PKGS[@]}"; do
    echo "INSTALLING: ${PKG}"
    sudo pacman -S "$PKG" --noconfirm --needed
done

echo
echo "Finished installing pacman packages!"
echo

# ------------------------------------------------------------------------

echo
echo "INSTALLING AUR PACKAGES"
echo

cd "$HOME"

echo "CLONING: YAY"
git clone "https://aur.archlinux.org/yay.git"


PKGS=(

    # UTILITIES -----------------------------------------------------------

    'shell-color-scripts'       # Shell color script
    'qtile-extras'              # Qtile extras
    'dmscripts-git'             # Dmenu scripts

)

cd $HOME/yay
makepkg -si

for PKG in "${PKGS[@]}"; do
    yay -S --noconfirm $PKG
done

echo
echo "Done installing yay packages!"
echo

# ------------------------------------------------------------------------


echo
echo "INSTALLING PIP PACKAGES"
echo

PKGS=(

    'dbus-next'                # Needed by qtile widget
    'psutil'                   # Needed by qtile widget

)

for PKG in "${PKGS[@]}"; do
    pip install $PKG
done

cd $HOME

echo
echo "Done installing pip packages!"
echo

# ------------------------------------------------------------------------

echo
echo "CLONING CONFIG FILES"
echo

    git clone https://gitlab.com/viperrnya/dotfiles
    cd $HOME/dotfiles
    cp -r $HOME/dotfiles/* $HOME
    cd $HOME

    source $HOME/.bashrc

echo
echo "Done!"
echo

# ------------------------------------------------------------------------

echo " (         (   (   (             (    (       ) (   (       )    (      ";
echo " )\ )  (   )\ ))\ ))\ )  *   )   )\ ) )\ ) ( /( )\ ))\ ) ( /(    )\ )   ";
echo "(()/(  )\ (()/(()/(()/(\` )  /(  (()/((()/( )\()|()/(()/( )\())( (()/(  ";
echo " /(_)|((_) /(_))(_))(_))( )(_))  /(_))/(_)|(_)\ /(_))(_)|(_)\ )\ /(_))  ";
echo "(_)) )\___(_))(_))(_)) (_(_())  (_))_(_))  _((_|_))(_))  _((_|(_|_))_   ";
echo "/ __((/ __| _ \_ _| _ \|_   _|  | |_ |_ _|| \| |_ _/ __|| || | __|   \  ";
echo "\__ \| (__|   /| ||  _/  | |    | __| | | | .\`|| |\__ \| __ | _|| |)|  ";
echo "|___/ \___|_|_\___|_|    |_|    |_|  |___||_|\_|___|___/|_||_|___|___/  ";
echo "                                                                        ";
